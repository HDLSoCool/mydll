﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClient
{
    public enum CmdID
    {
        CMD_ROBOTCLIENT_NEW = 0x00,     ///< 创建新的机器人客户端
        CMD_ROBOTCLIENT_DEL = 0x01,     ///< 注销一个机器人客户端
        CMD_ROBOTCLIENT_MOD = 0x02,      ///< 修改当前操作机器人
        CMD_ROBOTCLIENT_CONNECT = 0x11,  ///< 连接连接服务端
        CMD_ROBOT_SERVO_ON = 0x12,       ///< 设置伺服开
        CMD_ROBOT_SERVO_OFF = 0x13,      ///< 设置伺服关
        CMD_GET_ROBOTMOTION = 0x14,     ///< 获取机器人位置

        CMD_GET_IO_STATE = 0x30,		///< 获取IO状态	
        CMD_SET_IO_STATE = 0x31,		///< 设置IO状态	
        CMD_GET_PARAMETER = 0x32,		///< 获取机器人参数
        CMD_SET_PARAMETER = 0x33,		///< 设置机器人参数
        CMD_GET_FRAME = 0x34,		    ///< 获取坐标系
        CMD_SET_FRAME = 0x35,		    ///< 设置坐标系
        CMD_GET_ROBOT_STATE = 0x36,		///< 获取机器人状态

    }

    public enum ROBOTTYPE
    {
        ROBSOFT_SERIAL_SIX_CONVENTION,  // 六轴常规串联机型
        ROBSOFT_SERIAL_SIX_COOPERATION, // 六轴协作串联机型
        ROBSOFT_SERIAL_FOUR_CONVENTION, // 四轴常规串联机型，码垛
        ROBSOFT_SCARA_FOUR_ONERF,       // SCARA机器人，一轴可升降
        ROBSOFT_SCARA_FOUR_THREERF,     // SCARA机器人，三轴可升降
        ROBSOFT_SCARA_FOUR_FOURRF,      // SCARA机器人，四轴可升降
        ROBSOFT_DELTA_FOUR,             // DELTA机器人，单姿态
        ROBSOFT_DELTA_SIX,              // DELTA机器人，三姿态
        ROBSOFT_SERIAL_GENERAL,         // 通用串联机型
        ROBSOFT_CARTESIAN,              // 直角坐标机器人
    }


    public class RobotAddress
    {
        public string robotname { get; set; }
        public string ip { get; set; }
        public int port { get; set; }
        public string state { get; set; }
    }

    public class RobotParameter
    {
        public string Origin { get; set; }
        public int robot_dof { get; set; }
        public int robot_extern_dof { get; set; }
        public ROBOTTYPE robot_Type { get; set; }
        public double robot_Period { get; set; }

    }

    public class RobotMotion
    {
        public string Origin { get; set; }
        public double[] joint { get; set; }
        public double[] terminal { get; set; }
        public double[] terminal_work { get; set; }

        public double[] getCurrentJointPosition()
        {
            return joint;
        }

        public double[] getCurrentTerminal()
        {
            return terminal;
        }

        public double[] getCurrentWorkTerminal()
        {
            return terminal_work;
        }
    }
    public class RobotIO
    {
        public int[] io_do { get; set; }
        public int[] io_di { get; set; }
        public double[] io_ao { get; set; }
        public double[] io_ai { get; set; }
    }

    public class RobotState {
        public int state_run;         //运行状态
        public int state_servo;       //运行模式
        public int state_play;       //伺服状态
        public double state_vel;   // 机器人速度
    }
}
