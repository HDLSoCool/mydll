﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



using System.Runtime.InteropServices;
using UnityEngine;

namespace RobotClient
{
    //public class StringHelper
    //{
    //    public delegate string StringDelegate(string message);
    //    static StringDelegate stringDelegate = new StringDelegate(CreateString);
    //    // AlgoDemo为C++模块，RegisterStringCallback为创建字符串的注册回调函数
    //    [DllImport("RobotClient", EntryPoint = "RegisterStringCallback")]
    //    public static extern void RegisterStringCallback(StringDelegate stringDelegate);

    //    static string CreateString(string cString)
    //    {
    //        return cString;
    //    }

    //    static StringHelper()
    //    {
    //        RegisterStringCallback(stringDelegate);
    //    }
    //}

    public class RobotClientWebWrapper : MonoBehaviour
    {
        /// <summary>
        /// RobClientName
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>a+b</returns>
        //const string RobClientName = "liblws-minimal-ws-server";
        const string RobClientName = "libCWebExtern";

        [DllImport(RobClientName, EntryPoint = "RobotClientAdd")]
        private static extern double RobotClientAdd(double a, double b);

        [DllImport(RobClientName, EntryPoint = "NewRobot", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        private static extern void NewRobot(string name);

        [DllImport(RobClientName, EntryPoint = "DelRobot", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        private static extern void DelRobot(string name);

        [DllImport(RobClientName, EntryPoint = "Connect", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        private static extern bool Connect(string ip, int port);

        [DllImport(RobClientName, EntryPoint = "QueryFromUpper", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        private static extern int QueryFromUpper(int deviceID, string lpKey, ref string strResult);


        [DllImport(RobClientName, EntryPoint = "SetAuthority", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetAuthority(int authority);

        [DllImport(RobClientName, EntryPoint = "GetAuthority", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        private static extern void GetAuthority();

        [DllImport(RobClientName, EntryPoint = "InitLib", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        private static extern int InitLib(string name);

        private delegate string StringDelegate(string message);

        static StringDelegate stringDelegate = new StringDelegate(CreateString);

        // AlgoDemo为C++模块，RegisterStringCallback为创建字符串的注册回调函数
        [DllImport(RobClientName, EntryPoint = "RegisterStringCallback")]
        private static extern void RegisterStringCallback(StringDelegate stringDelegate);

        static string CreateString(string cString)
        {
            return cString;
        }

        private void Awake()
        {
            RegisterStringCallback(stringDelegate);
            //InitLib(@"D:\workspace\C++\RobotConSys\ExportApi\example\WebServerDemo\bin\Debug\RobotConSys_Client.dll");
            string path = @"D:\Test\Test_Data\Plugins\x86_64\RobotConSys_Client.dll";
            InitLib(path);
            Debug.Log("初始化库");
        }


        public bool DoConnect(string ip, int port)
        {
            return Connect(ip, port);
        }

        public void DoDelRobot(string robotName)
        {
            DelRobot(robotName);
        }

        public void DoNewRobot(string robotName)
        {
            NewRobot(robotName);
        }

        public void DoSetAuthority(int leve)
        {
            SetAuthority(leve);
        }
    }
}