﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RobotClient
{
    public class RobotClientWeb
    {
        public static int ID = 0;
        public string CmdReturn { get; set; }
        public string Origin
        {
            get
            {
                return JsonConvert.SerializeObject(_robot);
            }
            set { }
        }
        private RobotAddress _robot;


        private RobotClientWebWrapper m_RobotClientWeb;

        public RobotClientWeb(string IP, int Port, RobotClientWebWrapper robotClientWebWrapper)
        {
            _robot = new RobotAddress();
            _robot.ip = IP;
            _robot.port = Port;
            _robot.robotname = IP + "::" + Port.ToString();
            //string strRt = "";
            //RobotClientWebWrapper.QueryFromUpper((int)CmdID.CMD_ROBOTCLIENT_NEW, _robot.robotname, ref strRt);
            //CmdReturn = strRt;
            m_RobotClientWeb = robotClientWebWrapper;
            m_RobotClientWeb.DoNewRobot(_robot.robotname);
        }

        public void Dispose()
        {
            //string strRt = "";
            //RobotClientWebWrapper.QueryFromUpper((int)CmdID.CMD_ROBOTCLIENT_DEL, _robot.robotname, ref strRt);
            //CmdReturn = strRt;

            m_RobotClientWeb.DoDelRobot(_robot.robotname);
            m_RobotClientWeb = null;
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 连接初始化
        /// <para>连接时调用</para>
        /// </summary>
        /// <returns></returns>
        public bool initSystem()
        {
            //序列化成json
            //string resultJson = JsonConvert.SerializeObject(_robot);
            //string strResult = "";
            //bool bRt = RobotClientWebWrapper.QueryFromUpper((int)CmdID.CMD_ROBOTCLIENT_CONNECT, resultJson, ref strResult) == 1;
            //return bRt;
            return m_RobotClientWeb.DoConnect(_robot.ip, _robot.port);
        }

        private class RobotAddress
        {
            public string robotname { get; set; }
            public int port { get; set; }
            public string ip { get; set; }
        }
    }

}
